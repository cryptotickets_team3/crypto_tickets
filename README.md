## FaceBook Login
If you want facebook login work wuth current settings run app on `http://localhost:8011/` 


## ETH on OS X

1. Firstly you need get `geth` with ` bash <(curl -L https://install-geth.ethereum.org)`
2. We will use rpc to talk to `geth`, so run geth with `geth --rpc` (make sure eth wallet is closed)
3. To install `ethjsonrpc` package use: 
`sudo env LDFLAGS="-L$(brew --prefix openssl)/lib" CFLAGS="-I$(brew --prefix openssl)/include"  pip install git+https://github.com/ConsenSys/ethjsonrpc.git`
 And after get compiled version to local env with ussual: 
 `pip install ethjsonrpc`


# ETH testig
Use `python apps/eth/api.py` to test ETH possibilities. (NOTE: currentrly working only on `Python2` due to dependencies)

