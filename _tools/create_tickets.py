import datetime

from events.models import Ticket, Event

# generate some desent amount of tickets
amount = range(15, 30)
price = 6
ticket_type = 'FAN'

e = Event.objects.first()

for n in amount:
    print('creating ticket %s' % n)
    t = Ticket.objects.create(event=e, ticket_type='FAN', price=5,
                              expired=datetime.datetime.now(), place=n)
    t.save()
