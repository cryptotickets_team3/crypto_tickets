from django.contrib.auth.models import User

# creating superuser
login = 'admin'
password = 'admin123'
email = 'yanikkoval1@gmail.com'

User.objects.create_superuser(login, 'yanikkoval@gmail.com', password).save()

print("Admin user login: {}, password: {}".format(login, password))
