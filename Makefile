MANAGE=django-admin.py
SETTINGS=settings.base

migrate:
	PYTHONPATH=`pwd` DJANGO_SETTINGS_MODULE=$(SETTINGS) $(MANAGE) migrate

collectstatic:
	PYTHONPATH=`pwd` DJANGO_SETTINGS_MODULE=$(SETTINGS) $(MANAGE) collectstatic --noinput
.PHONY: test syncdb migrate

su:
	@echo "Creating superuser..."
	@python manage.py shell < _tools/create_user.py

tickets:
	@echo "Creating tickets..."
	@python manage.py shell < _tools/create_tickets.py

req:
	@echo "Installing requirements"
	@pip install --exists-action=s -r requirements.txt

shell:
	@echo "Running shell plus"
	@python manage.py shell_plus