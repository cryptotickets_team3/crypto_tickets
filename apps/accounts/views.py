from django.http import HttpResponseRedirect
from django.contrib.auth import logout as auth_logout
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.shortcuts import render_to_response, redirect
from django.contrib.messages.api import get_messages
from django.conf import settings

from facebook_auth.urls import redirect_uri

from .models import UserEthAdress


def login(request):
    # login_url = 'https://www.facebook.com/dialog/oauth?client_id={}&amp;scope={}&amp;redirect_uri={}'.format(
    #     settings.FACEBOOK_APP_ID, 'email', redirect_uri('/account/success', '/login/error')
    # )
    context = {
        'redirect_uri': redirect_uri('/account/success', '/login/error'),
        'client_id': settings.FACEBOOK_APP_ID,
        'scope': 'email'
    }
    return render_to_response('account/login.html', context)


def user_profile(request):
    pass


@login_required
def success(request):
    # Login complete view, displays user data
    ctx = {}
    return render_to_response('account/success.html', ctx, RequestContext(request))


def error(request):
    # Error view
    messages = get_messages(request)
    return render_to_response('account/error.html', {'messages': messages},
                              RequestContext(request))


def logout(request):
    # Logs out user
    auth_logout(request)
    return HttpResponseRedirect('/')
