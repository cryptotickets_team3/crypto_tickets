from django.contrib import admin

from .models import UserEthAdress


@admin.register(UserEthAdress)
class UserEthAdressAdmin(admin.ModelAdmin):
    pass