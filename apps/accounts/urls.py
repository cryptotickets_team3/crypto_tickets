from django.conf.urls import url
from django.contrib import admin

from .views import success, error, login


admin.autodiscover()


urlpatterns = [
    url(r'^login/$', login, name='login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'},
        name='logout'),
    url(r'^success/$', success, name='success'),
    url(r'^error/$', error, name='error'),
]