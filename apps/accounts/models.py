# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth import models as auth_models


class UserEthAdress(models.Model):
    user = models.OneToOneField(auth_models.User)
    adress = models.TextField(unique=True)
    # NOTE only for demo sake we save password as text
    adress_password = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return ' '.join([self.user.email, ' | ', self.adress])

    class Meta:
        verbose_name = 'User ETH'
        verbose_name_plural = 'User ETH adresses'

