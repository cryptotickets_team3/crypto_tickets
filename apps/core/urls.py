from django.conf.urls import url, include
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings


from .views import about, home

urlpatterns = [
    url(r'^admin/', admin.site.urls),

    # core
    url(r'^$', home, name='home'),
    url(r'^about$', about, name='about'),

    # apps
    url(r'^account/', include('accounts.urls', namespace='account')),
    url(r'^events/', include('events.urls', namespace='events')),

    # third party apps
    url(r'', include('facebook_auth.urls'))
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
