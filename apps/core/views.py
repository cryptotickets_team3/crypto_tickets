from django.views.generic import TemplateView


class HomeTemplateView(TemplateView):
    template_name = 'core/home.html'


class AboutTemplateView(TemplateView):
    template_name = 'core/about.html'


about = AboutTemplateView.as_view()
home = HomeTemplateView.as_view()
