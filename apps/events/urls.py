from django.conf.urls import url

from .views import events, event_detail, ticket_detail, ticket_buy


urlpatterns = [
    url(r'^all/$', events, name='list'),
    url(r'^detail/(?P<pk>[0-9]+)$', event_detail, name='detail'),
    url(r'^ticket/(?P<pk>[0-9]+)$', ticket_detail, name='ticket'),
    url(r'^ticket/(?P<pk>[0-9]+)/buy$', ticket_buy, name='ticket_buy')
]