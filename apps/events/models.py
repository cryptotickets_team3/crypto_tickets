# -*- coding: utf-8 -*-
from django.db import models


class Event(models.Model):
    title = models.CharField(max_length=255)
    starting_at = models.DateTimeField()

    def __str__(self):
        return self.title


class Ticket(models.Model):
    TICKET_TYPES = (
        # hardcoded for demo sake only
        # will be moved to separated model TicketType
        ('FAN', 'Fan Zone tickets'),
        ('VIP', 'VIP tickets')
    )

    ticket_type = models.CharField(choices=TICKET_TYPES, max_length=32)
    place = models.SmallIntegerField(null=True)
    price = models.SmallIntegerField(default=0)
    expired = models.DateTimeField()
    event = models.ForeignKey(Event, related_name='tickets', null=True, blank=True)
    available = models.BooleanField(help_text='available for sell', default=True)

    def __str__(self):
        return '{}, place {}, expired {}'.format(
            self.ticket_type, self.place, self.expired)