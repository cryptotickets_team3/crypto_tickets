# -*- coding: utf-8 -*-
from django.views.generic import ListView, DetailView
from django.shortcuts import redirect
from django.core.urlresolvers import reverse

from .models import Event, Ticket


class EventListView(ListView):
    model = Event
    context_object_name = 'events'
    template_name = 'events/list.html'
    ordering = ('-pk')


class EventDetailView(DetailView):
    model = Event
    context_object_name = 'event'
    template_name = 'events/detail.html'

    def get_context_data(self, **kwargs):
        context = super(EventDetailView, self).get_context_data(**kwargs)
        # grab all tickets associated with current event
        context['tickets'] = Ticket.objects.filter(event__id=self.object.id).order_by('place')
        return context


class TicketDetailView(DetailView):
    model = Ticket
    context_object_name = 'ticket'
    template_name = 'events/ticket.html'


def ticket_buy(request, pk):
    if request.method == 'POST':
        t = Ticket.objects.get(pk=pk)
        # TODO: send ticket to buyers adress
        t.available = False
        t.save()
        return redirect(reverse('events:ticket', kwargs={'pk': pk}))


events = EventListView.as_view()
event_detail = EventDetailView.as_view()
ticket_detail = TicketDetailView.as_view()
