from ethjsonrpc import EthJsonRpc

# will be imported from django.conf settings later
PROJECT_DIR = '/Users/yanik/code/crypto_tickets'


c = EthJsonRpc('127.0.0.1', 8545)
# make sure we connected
print('Welcon, you runung on version:', c.net_version())
# local coinbase:
# TEST-NET: 0xB14B05821c717c3B956aDad0c1bA8F3faC555B89 (red)
# other wallets
# 0x4aF3c94936dBa39CF1b7b95c05FC81c1813aEFb7 (first)
# 0xCfee88a952136B4e4BF687B3e332CF8cBA42aF2c (second)
print('Coinbase ', c.eth_coinbase())
print('Accounts', c.eth_accounts())

contract_path = PROJECT_DIR + '/contracts/sample_ticket.col'

# compile Solidarity contract
with open(contract_path, 'r') as contract:
    compiled = c.eth_compileSolidity(contract.read())
    compiled_code = compiled['Ticket']['code']
    # print(compiled_code)

# Deploy contract
# TODO: No chance we can deploy contrcat w/o unlocking the adress
# and we cant unlock through --rpc protocol

# contract_tx = c.create_contract(c.eth_coinbase(), compiled_code, gas=300000)
# contract_addr = c.get_contract_address(contract_tx)

